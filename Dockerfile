FROM alpine:3.5

RUN apk --no-cache add ca-certificates

WORKDIR /usr/local/bin

Add hello_world.sh .
CMD ["./hello_world.sh"]
